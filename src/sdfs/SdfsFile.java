package sdfs;

//We have to encrypt some of the metadata, so:
//
//public class SdfsFile implements java.io.Serializable {	
//
//	private byte[] data;
//	
//	private class SdfsFileMetaData implements java.io.Serializable {
//		private String ownerId;
//		private byte[]  hash
//	}
//	
//}	



public class SdfsFile implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6523254778307822965L;
	private String ownerId;
	private byte[] encryptedHash;
	private byte[] data;

	public SdfsFile(final String ownerId, final byte[] data) {
		this.ownerId = ownerId;
		this.data = data.clone();
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(final String ownerId) {
		this.ownerId = ownerId;
	}
	
	public boolean hasGetAccess(final String userId) {
		return this.ownerId.equals(userId);
	}
	
	public boolean hasPutAccess(final String userId) {
		return this.ownerId.equals(userId);
	}
	
	public void setData(final byte[] data) {
		this.data = data.clone();
	}

	public byte[] getEncryptedHash() {
		return encryptedHash.clone();
	}

	public void setEncryptedHash(final byte[] encryptedHash) {
		this.encryptedHash = encryptedHash.clone();
	}

	public byte[] getData() {
		return this.data.clone();
	}



}
