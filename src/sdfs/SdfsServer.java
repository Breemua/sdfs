package sdfs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;

import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

import messages.DelegationToken;
import messages.EndConnection;
import messages.GetCommand;
import messages.Ok;
import messages.PutCommand;

/**
 * Configuration to launch : 
 * 		Program arguments : 8081 files
 * 		VM arguments : -Djavax.net.ssl.keyStore="certs/SDFS_Server.jks" 
 * 					   -Djavax.net.ssl.keyStorePassword="hellohello"
 * 					   -Djavax.net.ssl.trustStore="certs/SDFS_CA.jks"
 * 					   -Djavax.net.ssl.trustStorePassword="hellohello"
 */
public class SdfsServer {

	private final transient String path;
	private transient Certificate certificate;
	private transient RSAPrivateKey privateKey;

	public SdfsServer(final String path) {

		this.path = path;

		try {

			KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());

			java.io.FileInputStream fis =
					new java.io.FileInputStream(System.getProperty("javax.net.ssl.keyStore"));
			keyStore.load(fis, System.getProperty("javax.net.ssl.keyStorePassword").toCharArray());
			fis.close();

			this.certificate = keyStore.getCertificate("certificate");
			this.privateKey = (RSAPrivateKey) keyStore.getKey("key", 
					System.getProperty("javax.net.ssl.keyStorePassword").toCharArray());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void startServer(int port) {
		System.out.println("Server listening on port "+port+ " and using file path "+path);
		final SSLServerSocketFactory ssfactory = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
		try {
			final ServerSocket serverSocket = ssfactory.createServerSocket(port);
			((SSLServerSocket)serverSocket).setNeedClientAuth(true);

			while (true) {
				new Thread(new RequestHandler((SSLSocket) serverSocket.accept())).start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private class RequestHandler implements Runnable {

		final transient private SSLSocket socket;

		public RequestHandler(final SSLSocket socket) {
			this.socket = socket;
		}

		@Override
		public void run() {
			try {
				final SSLSession session = ((SSLSocket) socket).getSession();
				final Certificate certificate = session.getPeerCertificates()[0];
				final String clientID = Utils.getIDfrom((X509Certificate) certificate);

				final ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
				final ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());

				boolean endCommunication = false;
				do {
					final Object input = inputStream.readObject();
					if(input instanceof GetCommand) {

						final String fileUID = ((GetCommand) input).getFileUID();
						final DelegationToken token = ((GetCommand) input).getToken(); 

						System.out.println("Received GET command of file "+fileUID);
						final byte[] decryptedData = getData(fileUID, clientID, token);
						if(decryptedData == null) {
							outputStream.writeObject(new Error(""));
						} else {
							outputStream.writeObject(decryptedData);
						}
					} else if (input instanceof PutCommand) {
						final String fileUID = ((PutCommand) input).getFileUID();
						System.out.println("Received PUT command of file "+fileUID);
						final byte[] data = ((PutCommand) input).getData();
						final DelegationToken token = ((PutCommand) input).getToken();

						if(putData(fileUID, clientID, token, data)) {
							outputStream.writeObject(new Ok(fileUID + " written.")); 
						} else {
							outputStream.writeObject(new Error("Error putting " + fileUID));
						}
					} else if (input instanceof EndConnection){
						endCommunication = true;
					}
				} while (!endCommunication);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public byte[] getData(final String fileUID, final String clientUID, final DelegationToken token) {
		final File file = getFile(fileUID);
		if(file != null) {
			try {
				final SdfsFile sdfsFile = getSdfsFile(file);
				if(clientUID.equals(sdfsFile.getOwnerId()) || 
						(token != null && token.canGet(clientUID, fileUID))) {

					return getDecryptedData(sdfsFile);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;		
	}

	private boolean putData(final String fileUID, final String clientUID, 
			final DelegationToken token, final byte[] data) {

		boolean result = false;

		try {

			final byte[] hash = Utils.hash(data, 16);
			final byte[] encryptedHash = Utils.encryptRSA(hash, privateKey);
			final byte[] encryptedData = Utils.encryptAES(data, new SecretKeySpec(hash, "AES"));

			File file = getFile(fileUID);
			SdfsFile sdfsFile;
			if(file == null) {
				sdfsFile = new SdfsFile(clientUID, encryptedData);
				file = new File(this.path + "/" + fileUID);
				result = true;
			} else {
				sdfsFile = getSdfsFile(file);
				if(clientUID.equals(sdfsFile.getOwnerId()) || 
						(token != null && token.canPut(clientUID, fileUID))) {

					sdfsFile.setData(encryptedData);
					result = true;
				}
			}
			if(result) {
				sdfsFile.setEncryptedHash(encryptedHash);
				final ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(file));
				outputStream.writeObject(sdfsFile);
				outputStream.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}

	private byte[] getDecryptedData(final SdfsFile sdfsFile) throws Exception {
		final byte[] encryptedHash = sdfsFile.getEncryptedHash();
		final byte[] decryptedHash = Utils.decryptRSA(encryptedHash, this.certificate.getPublicKey());
		return Utils.decryptAES(sdfsFile.getData(), new SecretKeySpec(decryptedHash, "AES"));

	}

	private File getFile(final String fileUID) {
		final File directory = new File(this.path);
		final File[] files = directory.listFiles();
		File file = null;
		for(File tmpFile : files) {
			if(tmpFile.getName().equals(fileUID)) {
				file = tmpFile;
				break;
			}
		}
		return file;
	}

	private SdfsFile getSdfsFile(final File file) throws FileNotFoundException, IOException, ClassNotFoundException {
		ObjectInputStream inputStream;
		inputStream = new ObjectInputStream(new FileInputStream(file));
		final SdfsFile sdfsFile = (SdfsFile) inputStream.readObject();
		inputStream.close();
		return sdfsFile;
	}


	public static void main(String[] args) {
		if(args.length != 2) {
			System.out.println("Server <server_port> <stored_files_path>");
		} else {
			SdfsServer sdfsServer = new SdfsServer(args[1]);
			sdfsServer.startServer(Integer.parseInt(args[0]));
		}
	}





}
