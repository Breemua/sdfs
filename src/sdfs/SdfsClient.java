package sdfs;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.security.cert.Certificate;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import messages.DelegationToken;
import messages.DelegationToken.Rights;
import messages.EndConnection;
import messages.GetCommand;
import messages.Ok;
import messages.PutCommand;

/* JVM launch example : -Djavax.net.ssl.keyStore="certs/SDFS_Client1.jks"   
                        -Djavax.net.ssl.keyStorePassword="hellohello"
                        -Djavax.net.ssl.trustStore="certs/SDFS_CA.jks"
                        -Djavax.net.ssl.trustStorePassword="hellohello" */

public class SdfsClient {

	private transient final String serverIp;
	private transient final int serverPort;
	private transient final int localPort;

	private transient SSLSocket socket;
	private transient ObjectOutputStream serverOutputStr;
	private transient ObjectInputStream serverInputStr;

	private final transient Map<String,DelegationToken> delegations;

	private transient Certificate certificate;
	private transient RSAPrivateKey privateKey;

	public SdfsClient(final String serverIp, final int serverPort,final int localPort) {
		super();

		try {

			final KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());

			final java.io.FileInputStream fis =
					new java.io.FileInputStream(System.getProperty("javax.net.ssl.keyStore"));
			keyStore.load(fis, System.getProperty("javax.net.ssl.keyStorePassword").toCharArray());
			fis.close();

			this.certificate = keyStore.getCertificate("certificate");
			this.privateKey = (RSAPrivateKey) keyStore.getKey("key", System.getProperty("javax.net.ssl.keyStorePassword").toCharArray());			

		} catch (Exception e) {
			e.printStackTrace();
		}

		this.serverIp=serverIp;
		this.serverPort=serverPort;
		this.serverOutputStr = null;
		this.serverInputStr = null;
		this.socket = null;
		this.localPort = localPort;
		this.delegations = new HashMap<String,DelegationToken>();
	}

	public void startLocalServer() {
		final ServerRunnable localServ = new ServerRunnable(this.localPort);
		new Thread(localServ).start();
	}

	public int startFsSession() {
		int retval=-1;
		final SSLSocketFactory factory = (SSLSocketFactory)SSLSocketFactory.getDefault();
		try {
			this.socket = (SSLSocket)factory.createSocket(this.serverIp, this.serverPort);
			this.socket.startHandshake();

			final SSLSession session = socket.getSession();
			final X509Certificate certif = (X509Certificate) session.getPeerCertificates()[0];
			final String identifier = Utils.getIDfrom(certif);

			if("SDFS_Server".equals(identifier)) {
				retval=1;
				this.serverOutputStr=new ObjectOutputStream(socket.getOutputStream());
				this.serverInputStr=new ObjectInputStream(socket.getInputStream());
			} 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retval;
	}

	public void get(final String UID) {

		System.out.println("Getting file "+UID);

		final GetCommand message;
		if (delegations.containsKey(UID)) {
			message = new GetCommand(UID,delegations.get(UID));
		} else {
			message = new GetCommand(UID);
		}

		try {
			this.serverOutputStr.writeObject(message);

			final Object returnmsg = this.serverInputStr.readObject();
			if(returnmsg instanceof Error) {
				final Error errormsg = (Error)returnmsg;
				final String errorstr = errormsg.getMessage();
				System.out.println("File GET error (file not found on server) : "+errorstr);

			} else if(returnmsg instanceof byte[]) {
				final byte[] returnfile = (byte[])returnmsg;
				final FileOutputStream fos = new FileOutputStream(UID);
				fos.write(returnfile);
				fos.close();
				System.out.println("File GET Ok: " + UID);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public void put(String file) {
		System.out.println("Uploading file "+file);
		try {
			final File fileToSend = new File(file);
			final BufferedInputStream bis = new BufferedInputStream(new FileInputStream(fileToSend));
			final byte[] yourBytes = new byte[(int)fileToSend.length()];
			bis.read(yourBytes);
			bis.close();

			String UID = fileToSend.getName(); 

			PutCommand message;
			if (delegations.containsKey(UID)) {
				message = new PutCommand(UID,yourBytes,delegations.get(UID));
			} else {
				message = new PutCommand(UID,yourBytes);
			}

			this.serverOutputStr.writeObject(message);	

			final Object returnmsg = this.serverInputStr.readObject();
			if(returnmsg instanceof Error) {
				final Error errormsg = (Error)returnmsg;
				final String errorstr = errormsg.getMessage();
				System.out.println("File PUT error : "+errorstr);

			} else if(returnmsg instanceof Ok) {
				System.out.println("File PUT accepted : "+((Ok)returnmsg).getMessage());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void delegate(final String UID, final String dest,final String destIP,final int port,final Rights rights,final long time, final boolean transitivity) {
		final SSLSocketFactory factory = (SSLSocketFactory)SSLSocketFactory.getDefault();
		try {
			SSLSocket socket = (SSLSocket)factory.createSocket(destIP, port);
			socket.startHandshake();

			final SSLSession session = socket.getSession();
			final X509Certificate certif = (X509Certificate) session.getPeerCertificates()[0];
			final String identifier = Utils.getIDfrom(certif);

			if(dest.equals(identifier)) {
				ObjectOutputStream serverOutputStr=new ObjectOutputStream(socket.getOutputStream());

				DelegationToken tok;
				if(delegations.containsKey(UID) && delegations.get(UID).isTransitiveAndCurrent()) {
					tok = delegations.get(UID);
					System.out.println("Delegating transitive token of file "+UID);
				} else {
					tok = new DelegationToken(UID,this.certificate,this.privateKey,dest,rights,time,transitivity);
					System.out.println("Delegating locally-owned file "+UID);
				}

				serverOutputStr.writeObject(tok);
				serverOutputStr.close();
			} 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void endSession() {
		try {
			this.serverOutputStr.writeObject(new EndConnection());
			this.serverInputStr.close();
			this.serverOutputStr.close();
			socket.close();
			System.out.println("Connection to the server closed");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

		if(args.length!=3) {
			System.out.println("Client <server_ip> <server_port> <local_port>");
		} else {
			final SdfsClient client = new SdfsClient(args[0],Integer.parseInt(args[1]),Integer.parseInt(args[2]));
			final int retval = client.startFsSession();
			client.startLocalServer();

			if(retval!=1) {
				System.out.println("Connection to the server didn't start correctly");
			} else if(retval==1) {
				//session started correctly
				System.out.println("Securely connected to server "+args[0]+":"+args[1]);
				final BufferedReader inBuffer = new BufferedReader(new InputStreamReader(System.in));
				try {
					String line="";
					while( (line = inBuffer.readLine()) != null) {
						if(line.length()>=5 && line.substring(0, 5).equals("close")) {
							client.endSession();
						} else if(line.length()>=3 && line.substring(0, 3).equals("put")) {
							client.put(line.substring(4));
						} else if(line.length()>=3 && line.substring(0,3).equals("get")) {
							client.get(line.substring(4));
						} else if(line.length()>=9 && line.substring(0,9).equals("delegate*")) {
							// delegate File Client Client_IP Client_Port Rights Time 
							System.out.println(line.substring(10));
							String[] remaining = line.substring(10).split(" ");

							if("PUT".equals(remaining[4])) {		
								client.delegate(remaining[0], remaining[1], remaining[2], Integer.parseInt(remaining[3]), Rights.PUT, Long.parseLong(remaining[5]), true);	
							} else if("GET".equals(remaining[4])) {
								client.delegate(remaining[0], remaining[1], remaining[2], Integer.parseInt(remaining[3]), Rights.GET, Long.parseLong(remaining[5]), true);			
							} else if("BOTH".equals(remaining[4])) {
								client.delegate(remaining[0], remaining[1], remaining[2], Integer.parseInt(remaining[3]), Rights.BOTH, Long.parseLong(remaining[5]), true);			
							} else {
								System.out.println(remaining[4]+" is not a valid delegation right (PUT/GET/BOTH)");
							}
						} else if(line.length()>=8 && line.substring(0,8).equals("delegate")) {
							// delegate lol.txt SDFS_Client1 8082 BOTH 100
							// delegate File Client Client_IP Client_Port Rights Time 
							String[] remaining = line.substring(9).split(" ");
							if("PUT".equals(remaining[4])) {	
								client.delegate(remaining[0], remaining[1], remaining[2], Integer.parseInt(remaining[3]), Rights.PUT, Long.parseLong(remaining[5]), false);
							} else if("GET".equals(remaining[4])) {				
								client.delegate(remaining[0], remaining[1], remaining[2], Integer.parseInt(remaining[3]), Rights.GET, Long.parseLong(remaining[5]), false);
							} else if("BOTH".equals(remaining[4])) {
								client.delegate(remaining[0], remaining[1], remaining[2], Integer.parseInt(remaining[3]), Rights.BOTH, Long.parseLong(remaining[5]), false);
							} else {
								System.out.println(remaining[4]+" is not a valid delegation right (PUT/GET/BOTH)");
							}
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private class ServerRunnable implements Runnable {

		private transient final int port;

		public ServerRunnable(final int port) {
			this.port = port;
		}

		@Override
		public void run() {
			try {

				final SSLServerSocketFactory ssfactory = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();

				final ServerSocket serverSocket = ssfactory.createServerSocket(port);
				((SSLServerSocket)serverSocket).setNeedClientAuth(true);

				while (true) {
					new Thread(new RequestHandler((SSLSocket) serverSocket.accept())).start();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

	private class RequestHandler implements Runnable {

		final transient private SSLSocket socket;

		public RequestHandler(final SSLSocket socket) {
			this.socket = socket;
		}

		@Override
		public void run() {
			try {
				final ObjectInputStream inputStream = new ObjectInputStream(this.socket.getInputStream());

				Object input = inputStream.readObject();

				synchronized(this) {
					if(input instanceof DelegationToken) {
						DelegationToken inputToken = ((DelegationToken) input);

						System.out.println("Received delegation token for file "+inputToken.getFileUID());

						String fileID = inputToken.getFileUID();
						delegations.put(fileID, inputToken);
					}
				}			

			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
