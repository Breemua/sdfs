package sdfs;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.security.spec.KeySpec;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;


public final class Utils {

	private static byte[] iv =
		{ 0x0a, 0x01, 0x02, 0x03, 0x04, 0x0b, 0x0c, 0x0d, 0x0a, 0x01, 0x02, 0x03, 0x04, 0x0b, 0x0c, 0x0d };

	private Utils(){}
	
	public static String getIDfrom(final X509Certificate cert) {
		final String subject = cert.getSubjectDN().getName();
		final String[] split = subject.split("[=,]");
	
		for(int i=0;i!=split.length-1;i++) {
			if(split[i].equals("CN")) {
				return split[i+1];
			}
		}
		return null;
	}

	public static byte[] encryptRSA(final byte[] inpBytes, final Key key) throws Exception {
		final Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		return cipher.doFinal(inpBytes);
	}

	public static byte[] decryptRSA(final byte[] inpBytes, final Key key) throws Exception{
		final Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE, key);
		return cipher.doFinal(inpBytes);
	}

	public static byte[] encryptAES(final byte[] inpBytes, final KeySpec keySpec) throws Exception {
		final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		final IvParameterSpec ips = new IvParameterSpec(iv);
		final SecretKeyFactory factory = SecretKeyFactory.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, factory.generateSecret(keySpec), ips);
		return cipher.doFinal(inpBytes);
	}

	public static byte[] decryptAES(final byte[] inpBytes, final KeySpec keySpec) throws Exception {
		final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		final IvParameterSpec ips = new IvParameterSpec(iv);
		final SecretKeyFactory factory = SecretKeyFactory.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE, factory.generateSecret(keySpec), ips);
		return cipher.doFinal(inpBytes);
	}

	public static byte[] hash(final byte[] input) {
		byte[] result = null;
	    try {
	    	final MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
	    	result = messageDigest.digest(input);
	    } catch(NoSuchAlgorithmException e) {
	        e.printStackTrace();
	    }
	    return result;
	}
	
	public static byte[] hash(final byte[] input, int size) {
		byte[] hash = Utils.hash(input);
		size = Math.min(size,hash.length);
	    byte[] realresult=new byte[size];
	    for(int i=0; i<size; i++) {
	    	realresult[i] = hash[i];
	    }
	    return realresult;
	}
	
	public static byte[] concatenate(final byte[] input1, final byte[] input2) {
		final int length1 = input1.length;
		final int length2 = input2.length;
		final byte[] concat = Arrays.copyOf(input1, length1 + length2);
		System.arraycopy(input2, 0, concat, length1, length2);
		return concat;
	}
	
	public static byte[] generateRandomBytes(final int size) {
		final SecureRandom random = new SecureRandom();
		final byte bytes[] = new byte[size];
		random.nextBytes(bytes);
		return bytes;
	}
	
	public static byte[] serialize(final Object obj) throws IOException {
	    final ByteArrayOutputStream out = new ByteArrayOutputStream();
	    final ObjectOutputStream os = new ObjectOutputStream(out);
	    os.writeObject(obj);
	    return out.toByteArray();
	}
	public static Object deserialize(final byte[] data) throws IOException, ClassNotFoundException {
		final ByteArrayInputStream in = new ByteArrayInputStream(data);
		final ObjectInputStream is = new ObjectInputStream(in);
	    return is.readObject();
	}
	
}
