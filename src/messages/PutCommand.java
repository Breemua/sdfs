package messages;

import java.io.Serializable;

public class PutCommand implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -266847514723729983L;
	
	private String fileUID;

	private byte[] data;
	
	private final DelegationToken token;
	
	public PutCommand(String fileUID, byte[] data) {
		this.fileUID = fileUID;
		this.data = data;
		this.token = null;
	}
	
	public PutCommand(String fileUID, byte[] data, DelegationToken token) {
		this.fileUID = fileUID;
		this.data = data;
		this.token = token;
	}
	
	public String getFileUID() {
		return this.fileUID;
	}
	
	public byte[] getData() {
		return this.data;
	}
	
	public DelegationToken getToken() {
		return this.token;
	}
}
