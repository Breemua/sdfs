package messages;

import java.io.IOException;
import java.io.Serializable;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Calendar;

import sdfs.Utils;

public class DelegationToken implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8290320108190481385L;

	public enum Rights {GET, PUT, BOTH};
	private final Certificate issuerCertificate;
	private final Content content;
	private byte[] contentHash;

	public DelegationToken(final String fileUID,final Certificate issuerCertificate, final PrivateKey issuerKey, 
			final String receiverUID, final Rights rights, final long time, final boolean transitivity) {

		this.issuerCertificate = issuerCertificate;
		this.content = new Content(fileUID,receiverUID, rights, time, transitivity);
		try {
			this.contentHash = Utils.encryptRSA(Utils.hash(this.content.toString().getBytes(), 16), issuerKey);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean isValid() {
		try {
			byte[] testHash = Utils.hash(this.content.toString().getBytes(), 16);
			byte[] decryptedHash = Utils.decryptRSA(this.contentHash, this.issuerCertificate.getPublicKey());
			if(Arrays.equals(testHash, decryptedHash)) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean isTransitiveAndCurrent() {
		return this.content.transitivity && (!this.content.hasExpired());
	}
	
	public Content getContent() {
		return this.content;
	}

	public String getID() {
		final Content content = getContent();
		return Utils.getIDfrom((X509Certificate) issuerCertificate.getPublicKey()) + 
				"_" + content.fileUID +
				"_" + content.rights;
	}
	
	public boolean canGet(final String clientUID, final String fileUID) {
		return isValid() && getContent().canGet(clientUID, fileUID);
	}

	public boolean canPut(final String clientUID, final String fileUID) {
		return isValid() && getContent().canPut(clientUID, fileUID);
	}
	
	public String getFileUID() {
		return this.getContent().fileUID;
	}

	private class Content implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -5218442041722131773L;
		private String fileUID;
		private final String receiverUID;
		private final Rights rights;
		private final long time;
		private final long start;
		private final boolean transitivity;

		public Content(final String fileUID,final String receiverUID, final Rights rights, 
				final long time, final boolean transitivity) {
			
			this.fileUID = fileUID;
			this.receiverUID = receiverUID;
			this.rights = rights;
			this.time = time;
			this.start = Calendar.getInstance().getTimeInMillis();
			this.transitivity = transitivity;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("fileUID=");
			sb.append(fileUID);
			sb.append(";\n");
			sb.append("receiverUID=");
			sb.append(receiverUID);
			sb.append(";\n");
			sb.append("rights");
			sb.append(rights);
			sb.append(";\n");
			sb.append("time=");
			sb.append(time);
			sb.append(";\n");
			sb.append("start=");
			sb.append(start);
			sb.append(";\n");
			sb.append("transitivity=");
			sb.append(transitivity);
			sb.append(";\n");
			return sb.toString();
		}
		
		public boolean hasExpired() {
			return start + time < Calendar.getInstance().getTimeInMillis();
		}

		public boolean canGet(final String clientUID, final String fileUID) {
			return this.fileUID.equals(fileUID) && 
					(rights == Rights.GET || rights == Rights.BOTH) &&
					((clientUID.equals(receiverUID)) || transitivity) &&
					!hasExpired();
		}

		public boolean canPut(final String clientUID, final String fileUID) {
			return this.fileUID.equals(fileUID) &&
					(rights == Rights.PUT || rights == Rights.BOTH) &&
					((clientUID.equals(receiverUID)) || transitivity) &&
					!hasExpired();
		}
		
	}
}
