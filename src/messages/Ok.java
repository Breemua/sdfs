package messages;

import java.io.Serializable;

public class Ok implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2979363318895390369L;

	private final String message;
	
	public Ok(final String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
	
}
