package messages;

import java.io.Serializable;

public class Error implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -128904075389781805L;
	
	private final String message;
	
	public Error(final String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
