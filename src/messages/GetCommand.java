package messages;

import java.io.Serializable;

public class GetCommand implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -266847514723729983L;
	
	private String fileUID;
	
	private DelegationToken token;
	
	public GetCommand(String fileUID) {
		this.fileUID = fileUID;
		this.token = null;
	}
	
	public GetCommand(String fileUID, DelegationToken token) {
		this.fileUID = fileUID;
		this.token = token;
	}
	
	public String getFileUID() {
		return this.fileUID;
	}
	
	public DelegationToken getToken() {
		return this.token;
	}
	
}
