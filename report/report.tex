% Page style
\documentclass[11pt, pdftex]{article}
\usepackage{epsf}
\usepackage{epsfig}
\usepackage{times}
\usepackage{ifthen}
\usepackage{comment}
\usepackage[margin=1in]{geometry}
\usepackage{graphicx}



\title{CS 6238 - Secure Computer Systems \vspace{0.7cm} \\ \Large{Project 2: Secure Distributed File System (SDFS)}}
\author{\textsc{Erb} Guillaume \and \textsc{Palandri} R{\'e}mi}
\date{}

\begin{document}
\maketitle


\section*{Introduction}
SDFS is a secure distributed file system that permits its users to share files with others and store them on a server securely. Our system also enables its users to delegate for a certain amount of time some or all of their rights to an other user regarding a defined file. This system is entirely secure, as all the communications between users and with the server are encrypted using the SSL technology, the files are stored on the server in an encrypted way, and the server and clients are authentified using a certificate signed by a trusted third party. \\
We implemented this application using the Java programming language and tested it using the \textit{PMD} and \textit{FindBugs} tools.

\section{Implementation}
\subsection{Tools and Libraries}
For this project, we used the SSL java implementation available in the \textit{SSLSockets} class. We also used the \textit{OpenSSL} toolkit to create a Certification Authority, private-public key pairs and certificates for every client and the server of our application. Thanks to the \textit{keytool} command utility, we added these keys and certificates to \textit{.jks} Java Key Store files that are loaded as JVM parameters with our application. \\
We limited our use of Java functions to the fully tested functions of the Java Development Kit included in the \textit{java.security} and \textit{javax.net.ssl} packages, to minimize the chance to call a non-trusted function in an external \textit{.jar} file. 

\subsection{Launching the application}
\begin{itemize}
\item The server : It needs 2 arguments, the receive port and the path of the directory in which it will save the encrypted versions of the files.
\item The client : It needs 3 arguments, the server IP, the server port, and the local port the client will listen on to receive delegation tokens. 
\item The JVM : It needs 4 arguments, the keyStore to use, its password, the trustStore to use and its password. 
\end{itemize}
To facilitate the tests, we created 4 Bash scripts : \textit{server.sh} launches the server on the port \textit{8081} , and \textit{client1/2/3.sh} launches clients whose receive ports are \textit{8082}, \textit{8083} and \textit{8084}.

\subsection{Basic Commands}
The different commands of our application are :
\begin{itemize}
\item \begin{verbatim}put filePath\end{verbatim} uploads \textit{filePath} to the server.
\item \begin{verbatim}get filePath\end{verbatim} retrieves \textit{filePath} from the server.
\item \begin{verbatim}delegate filePath clientID clientIP clientPort Rights time\end{verbatim} creates a delegation token for file \textit{filePath} to client \textit{clientID} with rights \textit{Rights} for a time \textit{time} and send it to him to \textit{clientIP:clientPort}. 
\item \begin{verbatim}close\end{verbatim} closes the connection to the server.
\end{itemize}

\section{Protocols}

\subsection{Messages}
\begin{itemize}
\item \textit{PUT command} :  sends a \textit{PUT} message to the server. It contains a file name, an array of bytes containing the unencrypted file, and possibly a delegation token. The server will answer by either an \textit{Error} message containing the reason of the failure, or an \textit{Ok} message.
\item \textit{GET command} : sends a \textit{GET} message to the server. It contains a file name and possibly a delegation token. The server will answer by either an \textit{Error} message containing the reason of the failure, or an array of bytes containing the unencrypted files.
\item \textit{Delegation creation} : creates a delegation token and sends it using a \textit{DelegationToken} message containing the token. Receives a \textit{Ok} message if the receiver accepted the token, or an \textit{Error} message otherwise.
\item \textit{Connection closure} : closes the connection to the server by sending an \textit{EndConnection} message to the server. 
\end{itemize}

\subsection{Secure Sockets Layer (SSL)}
Our implementation of the secure communications uses 2 \textit{.jks} files for each actor of the distributed file system, the \textit{TrustStore} file and the \textit{KeyStore} file. The \textit{TrustStore} file contains the Certificate of the trusted third party that has been used to sign all the other certificates. The \textit{KeyStore} file contains the certificate of the application (client or server) and its private key. These informations are used to set-up the SSL session used in the SSL sockets. 

When a client connects to the server, he verifies first during the SSL handshake that his interlocutor's certificate has been signed by the CA. He then verifies that his interlocutor's alias is the server's one (defined in our application as \textit{SDFS\_Server}) to be sure that no one can intercept nor receive the communications intended for the server). The same procedure is done for client/client communication during the delegation process. 

\subsection{Delegation protocol}
During the delegation part, the client who delegates a right creates a delegation token that, when sent alongside to a request to the server, will grant an other client authorization to execute the request. 
This token contains several variables to precisely define its capabilities:
\begin{itemize}
\item The identifier of the concerned file.
\item The issuer's certificate.
\item The identifier of the client concerned by the delegation.
\item The rights given to the receiver of the token (authorized to execute a \textit{PUT} command, a \textit{GET} command, or both).
\item The time of validity of this token (measured in ms).
\item The transitivity of this token (can the receiver of this token further delegate the file's rights to someone else?).
\end{itemize}

The token also contains a hash of its contents, encrypted using the issuer's private key. When this token is used by a client to access the concerned file, the client sends this token to the server, who verifies that the contents of the token haven't been modified by simply calculating the content hash, and comparing it with the decrypted value of the hash contained in the token (decrypted using the public key contained in the issuer's certificate).

If they match, it means that the token was forged by its issuer : no one else could encrypt this hash, as only the issuer knows its private key.

After a delegation token is created, it is sent directly to the client using a client-client SSL connection similar to the one connecting the clients to the server. We considered that all the clients know the IP addresses and client-client connection ports of the other clients, as this is supposed to be a simplified version of a real system. In a real system, the server would maintain an IP/port list containing the informations of every client and transmit this list to its clients when they have a delegation token to create.

One of this delegation system's biggest strengths is scalability, as the server doesn't have to save nor receive any information about the different delegations a file can be subjected to. For the server, a file only has a single owner, and he will check the validity of received delegations tokens alongside with the requests coming from other clients.

\subsection{Rights management}

When a file is created and first sent to the server using a \textit{PUT} command, the file is created on the server and its owner set to the SSL identifier of the sender. The owner of a file is immutable : even if the owner creates delegation tokens, he will always remain the only owner.

When a file request is received by the server (\textit{PUT} or \textit{GET}), the following flowchart is executed. It doesn't show the differences between the PUT and GET commands, but these are trivial : a file has 2 types of rights, \textit{PUT} and \textit{GET} rights : 
\begin{itemize}
\item The \textit{PUT} right enables a client to overwrite the contents of the file on the server
\item The \textit{GET} right enables a client to retrieve the contents of the file stored on the server
\end{itemize}
An owner has automatically both rights to its files, and a delegation token contains the rights a client has towards its target file, \textit{PUT}, \textit{GET} or \textit{BOTH}.

\includegraphics[scale=0.40]{flowchart.png}

\subsection{File Storage}
When a file is uploaded to the server using the \textit{PUT} command, it is encrypted then written on the hard drive. The encryption key is simply a hash of the original file, but as the original file is not stored on the hard drive, no one can recover it. The encryption key is then encrypted using the server's public key, and stored alongside the file. To decrypt the file, the encryption key is needed : only the server can recover it, as it is the only one to have the necessary private key. 

\section{Performance}

\begin{itemize}
\item Startup time:
\item File Transfer time:
\end{itemize}


\section*{Conclusion}
Our application implements a completely secure filesystem that permits several clients to store their files on a distant server both with secure communications, thanks to a SSL session, and also secure storage.
We can also see that its performances are completely acceptable, enabling our system to be used in a real-time file-sharing environment.

\end{document}
